# uralsib-task

## node_modules в архиве
https://gitlab.com/uralsib-test/uralsib-task/uploads/04123da666f6be68dee9fdad5985c78e/node_modules.rar

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
