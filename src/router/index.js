import VueRouter from "vue-router";
import Task_1 from "@/components/task/Task_1";
import Task_2 from "@/components/task/Task_2";
import Task_3 from "@/components/task/Task_3";

export default new VueRouter({
    routes: [
        {
            name: "task-1",
            path: "/task-1",
            meta: {name:"Задание №1"},
            component: Task_1
        },
        {
            name: "task-2",
            path: "/task-2",
            meta: {name:"Задание №2"},
            component: Task_2
        },
        {
            name: "task-3",
            path: "/task-3",
            meta: {name:"Задание №3"},
            component: Task_3
        }
    ]
})