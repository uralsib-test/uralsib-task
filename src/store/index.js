import Vuex from 'vuex';
import Vue from "vue";
import taskModule from "@/store/modul/taskModule";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        taskModule
    }
});