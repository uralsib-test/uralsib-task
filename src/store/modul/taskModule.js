import http from "@/connector"
import Product from "@/model/Product";

export default {
    namespaced: true,
    state: {
        product: null,
    },
    getters: {
        /**
         * @param state
         * @returns {Product}
         */
        product: state => state.product,
    },
    mutations: {
        setProduct(state, product) {
            state.product = product;
        }
    },
    actions: {
        /**
         * Запрос продукта по ID
         *
         * @param _context
         * @param id идентификатор продукта
         * @returns {Promise<Product>}
         */
        productRequest({commit}, id) {
            return new Promise((resolve) => {
                http.get(`https://fakestoreapi.com/products/${id}`).then(
                    (response) => {
                        let product = new Product(response.data)
                        commit('setProduct', product)

                        resolve(product)
                    }
                )
            })
        }
    }
}