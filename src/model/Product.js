export default class Product {
    constructor(data = {}) {
        this.id = data.id;
        this.title = data.title;
        this.price = data.price;
        this.description = data.description;
        this.category = data.category;
        this.image = data.image;
        this.rating = data.rating;
    }
}